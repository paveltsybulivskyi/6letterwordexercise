using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace SixLetters.Tests {
  public class UnitTest1 {
    [Fact]
    public void CreateTargetWord_With_Params() {
      // Arrange

      var targetWordStr = "letter";

      var otherWords = new[] { "l", "et", "bo", "ter" };

      // Act
      var targetWord = new TargetWord(targetWordStr, otherWords);

      // Assert
      targetWord.Word.Should().Equals(targetWordStr);
      targetWord.RelatedPartials.Length.Should().Equals(3);
      targetWord.RelatedPartials.Should().Contain(otherWords.Except(new[]{"bo"}));
    }
  }
}