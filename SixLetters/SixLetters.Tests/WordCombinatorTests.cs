﻿using System.Linq;
using FluentAssertions;
using Xunit;

namespace SixLetters.Tests {
  public class WordCombinatorTests {
    [Fact]
    public void FindWordCombination() {
      // Arrange
      var targetWord = new TargetWord("letter", new[] { "le", "t", "er", "let", "ter" });

      // Act
      var wordCombinator = new WordCombinator(2);
      var wordCombinations = wordCombinator.FindWordCombination(targetWord);

      // Assert
      wordCombinations.Length.Should().Equals(1);
      var firstCombination = wordCombinations[0];
      firstCombination.Combination.Should().Equals("let+ter");
      firstCombination.ResultWord.Should().Equals("letter");
    }

    [Fact]
    public void FindWordCombinationsFor() {
      // Arrange
      var targetWords = new[] {
        new TargetWord("letter", new[] { "le", "t", "er", "let", "ter" }),
        new TargetWord("butter", new[] { "b", "utter", "er", "let", "ter" })
      };

      // Act
      var wordCombinator = new WordCombinator(2);
      var wordCombinations = wordCombinator.FindCombinationsFor(targetWords);

      // Assert
      wordCombinations.Length.Should().Equals(2);
      var firstCombination = wordCombinations.First(x => x.ResultWord == "butter");
      firstCombination.Combination.Should().Equals("b+utter");
      firstCombination.ResultWord.Should().Equals("butter");
    }
  }
}