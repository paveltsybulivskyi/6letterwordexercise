﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace SixLetters {
  public class WordCombinator {
    private readonly int _numberOfPartials;

    public WordCombinator(int numberOfPartials) {
      _numberOfPartials = numberOfPartials;
    }

    public ImmutableArray<WordCombination> FindWordCombination(TargetWord targetWord) {
      var multiPartials = WordIteration(targetWord.RelatedPartials, _numberOfPartials);
      var combinedPartials = CartesianProduct(multiPartials);
      var matches = combinedPartials
        .Where(cw => cw.ResultWord == targetWord.Word)
        .ToImmutableArray();

      return matches;
    }

    private IEnumerable<IEnumerable<string>> WordIteration(IEnumerable<string> seq, int iteration) {
      for (int i = 0; i < iteration; i++) {
        yield return seq;
      }
    }

    private IEnumerable<WordCombination> CartesianProduct(IEnumerable<IEnumerable<string>> wordsCollection) {
      var result = new[] { new WordCombination() }.AsEnumerable();

      foreach (var simpleWords in wordsCollection) {
        result =
          from prevCombination in result
          from word in simpleWords
          select new WordCombination {
            ResultWord = prevCombination.ResultWord + word,
            Combination = prevCombination.Combination != null ? $"{prevCombination.Combination}+{word}" : word
          };
      }

      return result;
    }

    public ImmutableArray<WordCombination> FindCombinationsFor(IEnumerable<TargetWord> targetWords) {
      var resultMatches = new ConcurrentBag<WordCombination>();

      Parallel.ForEach(targetWords, word => {
        var matches = FindWordCombination(word);

        foreach (var match in matches) {
          resultMatches.Add(match);
        }
      });

      return resultMatches.ToImmutableArray();
    }
  }
}