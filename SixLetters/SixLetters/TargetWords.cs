﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SixLetters {
  public class TargetWords {
    public static IEnumerable<TargetWord> CreateFrom(string fileName, int targetWordLength) {
      var allWords = File.ReadAllLines(fileName).Distinct().ToList();
      var targetWords = CreateTargetWords(allWords, targetWordLength);

      return targetWords;
    }

    public static IEnumerable<TargetWord> CreateTargetWords(IReadOnlyCollection<string> allWords, int targetWordLength) {
      var targetWordStrings =
        allWords
          .Where(x => x.Length == targetWordLength);

      foreach (var targetWord in targetWordStrings) {
        yield return new TargetWord(targetWord, allWords);
      }
    }
  }
}