﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace SixLetters {
  public class TargetWord {
    public TargetWord(string targetWord, IEnumerable<string> allWords) {
      Word = targetWord;
      RelatedPartials = allWords
        .Where(w => w != targetWord && targetWord.Contains((string)w))
        .ToImmutableArray();
    }

    public string Word { get; set; }

    public ImmutableArray<string> RelatedPartials { get; set; }
  }
}