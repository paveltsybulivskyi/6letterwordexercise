﻿using System;
using System.Collections;
using System.ComponentModel;

// TODO: Perform benchmark to find spots that can be improved to reduce memory allocations
// TODO: think about more optimal algorithm
// TODO: add more unit tests
// TODO: probably reimplement in functional way using F#
namespace SixLetters {
  class Program {
    static void Main(string[] args) {
      Console.WriteLine("6 Letter word exercise");
      var targetWordLength = UserInput("Please enter the length of the target word", defaultValue: 6);
      var numberOfPartials = UserInput("Please specify how many words should be combined to form a target word:", defaultValue: 2);

      var targetWords = TargetWords.CreateFrom("input.txt", targetWordLength);

      var result = new WordCombinator(numberOfPartials).FindCombinationsFor(targetWords);

      foreach (var match in result) {
        Console.WriteLine($"{match.Combination}={match.ResultWord}");
      }

      UserInput("Enter 0 or press Enter for exist", 0);
    }

    public static int UserInput(string prompt, int defaultValue) {
      while (true) {
        Console.Write(prompt + $" [{defaultValue}]");
        var input = Console.ReadLine();

        if (string.IsNullOrEmpty(input))
          return defaultValue;

        if (int.TryParse(input, out var parsedValue)) {
          return parsedValue;
        }
      }
    }
  }
}