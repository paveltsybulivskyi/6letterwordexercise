﻿namespace SixLetters {
  public struct WordCombination {
    public string ResultWord { get; set; }
    public string Combination { get; set; }
  }
}